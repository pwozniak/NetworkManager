Run NetworkManager in Container for Development
===============================================

Run `nm-in-container` script for running NetworkManager in a podman container.
This is useful for testing NetworkManager for development.

Try `./nm-in-container --help` to see options.  See the bash history inside the
container for useful commands.
